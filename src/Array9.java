import java.util.Scanner;

public class Array9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int array[] = new int[3];
        for (int i = 0;i <array.length;i++){
            System.out.print("Please input arr["+i+"]: ");
            array[i] = scanner.nextInt();
        }
        System.out.print("arr = ");
        for (int i = 0;i <array.length;i++){
            System.out.print(array[i]+" ");
        }
        System.out.println();
        System.out.print("Please input search value:");
        int num = scanner.nextInt();
        int index = -1;
        for (int i = 0;i <array.length;i++){
            if (array[i]==num) {
                index = i;
                break;
            }
        } 
        if (index >= 0) {
            System.out.println("found at index:"+index);
            
        }
        else{
            System.out.println("Not found");
        }

    scanner.close();

    }
}
