import java.util.Scanner;

public class Array7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int array[] = new int[3];
        for (int i = 0;i <array.length;i++){
            System.out.print("Please input arr["+i+"]: ");
            array[i] = scanner.nextInt();
        }
        System.out.print("arr = ");
        int sum = 0;
        double avg ;
        for (int i = 0;i < array.length;i++){
            System.out.print(array[i]+" ");
            sum += array[i];
        }
        avg = sum/array.length;
        System.out.println();
        System.out.println("sum = "+sum);
        System.out.println("avg = "+avg);
        int index = 0;
        for (int i = 0;i < array.length;i++){
            if (array[index]>array[i]) {
                index = i;
            }
        }
        System.out.println("min = "+array[index]);


    scanner.close();

    }
}
