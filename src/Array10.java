import java.util.Scanner;

public class Array10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input number of element: ");
        int num = scanner.nextInt();
        int array[] = new int[num];
        int unisize = 0;
        for (int i = 0;i < array.length;i++){
            System.out.print("Element "+i+":");
            int temp = scanner.nextInt();
            int index = -1;
            for (int j = 0;j < unisize;j++ ){
                if (array[j]==temp) {
                    index = j;
                }
            }
            if (index < 0) {
                array[unisize] = temp;
                unisize++;
            }
        }
        System.out.print("arr = ");
        for (int i = 0;i <unisize;i++){
            System.out.print(array[i]+" ");
        } 

    }
}
