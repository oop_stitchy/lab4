import java.util.Scanner;

public class Array5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int array[] = new int[3];
        for (int i = 0;i <array.length;i++){
            System.out.print("Please input arr["+i+"]: ");
            array[i] = scanner.nextInt();
        }
        System.out.print("arr = ");
        int sum = 0;
        for (int i = 0;i < array.length;i++){
            System.out.print(array[i]+" ");
            sum += array[i];
        }
        System.out.println();
        System.out.println("sum = "+sum);
    scanner.close();

    }
}
